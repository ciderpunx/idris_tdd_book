module Palindrome
import Data.String -- for toLower

palindrome : String -> Bool
palindrome xs = xs == reverse xs

palindrome' : String -> Bool
palindrome' = palindrome . toLower

palindrome'' : String -> Bool
palindrome'' xs = length xs > 10 && palindrome' xs

palindrome''' : Nat -> String -> Bool
palindrome''' len xs =
  length xs > len && palindrome' xs

xs = "abba"
