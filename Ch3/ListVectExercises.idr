import Data.Vect

-- I suppose you ought to use a fold here really but this is nice and simple
total my_length : List a -> Nat
my_length [] = 0
my_length (x :: xs) = 1 + my_length xs

total my_reverse : List a -> List a
my_reverse [] = []
my_reverse (x :: xs) = my_reverse xs ++ [x]

total my_reverse' : List a -> List a
my_reverse' xs = revl [] xs
  where
    revl : List a -> List a -> List a
    revl acc [] = acc
    revl acc (y :: ys) = revl (y :: acc) ys

total my_list_map : (a -> b) -> List a -> List b
my_list_map f [] = []
my_list_map f (x :: xs) = f x :: my_list_map f xs

total my_vec_map : (a -> b) -> Vect len a -> Vect len b
my_vec_map f [] = []
my_vec_map f (x :: xs) = f x :: my_vec_map f xs


