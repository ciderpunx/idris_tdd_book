import Data.Vect

createEmpties : {n : _ } -> Vect n (Vect 0 elt)
createEmpties = replicate _ []

transposeHelper : (x : Vect n elt)
          -> (xsTrans : Vect n (Vect len elt)) 
          -> Vect n (Vect (S len) elt)
transposeHelper [] [] = []
transposeHelper (x :: xs) (y :: ys) = (x :: y) :: transposeHelper xs ys


transposeMat : { n : _} -> Vect m (Vect n elt) -> Vect n (Vect m elt)
transposeMat [] = createEmpties
transposeMat (x :: xs) = let xsTrans = transposeMat xs in
  zipWith (::) x xsTrans

addMat : Num a => Vect m (Vect n a)
        -> Vect m (Vect n a)
        -> Vect m (Vect n a)
addMat [] [] = []
addMat (x :: xs) (y :: ys) =
  zipWith (+) x y :: addMat xs ys

total mulMat : Num a =>
            {m : _ }
         -> {p : _ }
         -> Vect m (Vect n a)
         -> Vect n (Vect p a)
         -> Vect m (Vect p a)
mulMat [] rs = []
mulMat {n} ls rs =
  let
    rss = replicate m (transposeMat rs)
  in
    zipWith
      (\l', rs' => map (sum . zipWith (*) l') rs')
      ls
      rss

mulMatTest =
  mulMat [[1,2], [3,4], [5,6]]
         [[7,8,9,10], [11,12,13,14]]

mulMatTestOK = mulMatTest == 
  [[29,32,35,38],
   [65,72,79,86],
   [101,112,123,134]
  ]
