import Data.Vect

-- Append with /explicit/ elt, n, m types
append : (elt : Type)
          -> ( n : Nat)
          -> ( m : Nat)
          -> Vect n elt
          -> Vect m elt
          -> Vect (n+m) elt
append elt Z m  [] ys = ys
append elt (S k) m (x::xs) ys = x :: append elt k m xs ys


-- But probably easier to read with /implicit/ args
-- In this case n,m,elt are UNBOUND
append' : Vect n elt -> Vect m elt -> Vect (n + m) elt
append' [] ys = ys
append' (x::xs) ys = x :: (append' xs ys)

-- We can also BIND the implicits on type eg.
-- normally don't, but you might want to for clarity or need to when 
-- Idris can't infer the type
append'' : {elt : Type} 
  -> {n: Nat}
  -> {m: Nat}
  -> Vect n elt
  -> Vect m elt
  -> Vect (n + m) elt
append'' = append'
