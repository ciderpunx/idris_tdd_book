module WordLength

allLengths : List String -> List Nat
allLengths [] = []
allLengths (w :: ws) = length w :: allLengths ws

xor : Bool -> Bool -> Bool
xor False y = y
xor True y = not y

mutual
  isEven : Nat -> Bool
  isEven Z = True
  isEven (S n) = isOdd n

  isOdd : Nat-> Bool
  isOdd Z = False
  isOdd (S n) = isEven n
