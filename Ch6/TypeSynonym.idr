import Data.Vect

tri' : Vect 3 (Double, Double)
tri' = [(0.0, 0.0), (3.0, 0.0), (0.0,4.0)]

--We can use Position as a synonmy for (Double, Double)
Position : Type
Position = (Double, Double)

Polygon : Nat -> Type
Polygon n = Vect n Position

tri : Polygon 3
tri = [(0.0, 0.0), (3.0, 0.0), (0.0,4.0)]

tri'' : Polygon 3
tri'' = [(0.0, 0.0), (3.0, 0.0), (0.0,4.0)]
