data Format = Number Format
            | Dbl Format
            | Str Format
            | Chr Format
            | Lit String Format
            | End

PrintfType : Format -> Type
PrintfType (Number fmt)  = (i:Int) -> PrintfType fmt
PrintfType (Dbl fmt)     = (f:Double) -> PrintfType fmt
PrintfType (Str fmt)     = (str : String) -> PrintfType fmt
PrintfType (Chr fmt)     = (chr : Char) -> PrintfType fmt
PrintfType (Lit str fmt) = PrintfType fmt
PrintfType End           = String

-- given a list of characters, give me a Format
-- Formats can recursively contain other formats...
toFormat : (xs : List Char) -> Format
toFormat [] = End
toFormat ('%' :: 'd' :: chars) = Number $ toFormat chars
toFormat ('%' :: 'f' :: chars) = Dbl $ toFormat chars
toFormat ('%' :: 's' :: chars) = Str $ toFormat chars
toFormat ('%' :: 'c' :: chars) = Chr $ toFormat chars
toFormat ('%' :: chars)        = Lit "%" (toFormat chars)
toFormat (c :: chars) =
  case toFormat chars of
       Lit lit chars' => Lit (strCons c lit) chars'
       fmt            => Lit (strCons c "") fmt

-- Given a Format, build a String with the fomat plus any necessary 
-- extra args
printfFmt : (fmt : Format) -> (acc : String) -> PrintfType fmt
printfFmt (Number fmt) acc  = \i => printfFmt fmt (acc ++ show i)
printfFmt (Dbl fmt) acc     = \f => printfFmt fmt (acc ++ show f)
printfFmt (Str fmt) acc     = \s => printfFmt fmt (acc ++ s)
printfFmt (Chr fmt) acc     = \c => printfFmt fmt (strCons c acc)
printfFmt (Lit lit fmt) acc = printfFmt fmt (acc ++ lit)
printfFmt (End) acc         = acc


-- Now we can do this sort of thing:
-- :t printf "%c %f %d %s %f %d %x"
-- printf "%c %f %d %s %f %d %x" : Char -> Double -> Int -> String -> Double -> Int -> String
printf : (fmt : String) -> PrintfType (toFormat (unpack fmt))
printf fmt = printfFmt _ "" -- uses type inference to work out what _ must be (via toFormat)
