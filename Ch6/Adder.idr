-- The idea now is that we can create types based on their number of args
-- using a type-level function. Convention is to capitalize these.


-- We ignore the fact that numargs is not the number of args but the last index
-- of the list of args
AdderType : (numargs : Nat) -> Type -> Type
AdderType Z numType = numType
AdderType (S k) numType = (next : numType) -> AdderType k numType


adder : Num numType =>
        (numargs : Nat) -> (acc : numType) -> AdderType numargs numType
adder Z acc = acc
adder (S k) acc = \next => adder k (next + acc)
