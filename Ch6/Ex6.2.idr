-- Ex 6.2.2.1
import Data.Vect

Matrix : Nat -> Nat -> Type
Matrix n m = Vect n (Vect m Double)

testMatrix : Matrix 2 3
testMatrix = [ [0,0,0], [0,0,0]]

-- Ex6.2.2.3
TupleVect : (len : Nat) -> Type -> Type
TupleVect 0 ty = ()
TupleVect (S k) ty = (ty, TupleVect k ty)

test : TupleVect 4 Nat
test = (1,2,3,4,())

-- Loaded file Ex6.2.idr
-- :t test
-- Main.test : TupleVect 4 Nat
-- test
-- (1, (2, (3, (4, ()))))
