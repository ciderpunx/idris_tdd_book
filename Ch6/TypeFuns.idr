import Data.String
-- Type synonyms are an instance of type-level functions...

StringOrInt : Bool -> Type
StringOrInt False = String
StringOrInt True = Integer

getStringOrInt : (isInt : Bool) -> StringOrInt isInt
getStringOrInt False = "Ninety four"
getStringOrInt True = 94

valToString : (isInt : Bool) -> StringOrInt isInt -> String
valToString False x = trim x
valToString True x = show x

-- You can use a case expr in the type of valToString
-- So we can have a function that takes a type that is
-- used to calculate what types it will accept as input.
valToString' : (isInt : Bool)
  -> (case isInt of
           False => String
           True => Integer)
  -> String
valToString' False x = trim x
valToString' True x = show x
