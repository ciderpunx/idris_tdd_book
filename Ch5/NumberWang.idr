module NumberWang

import Data.List
import Data.String
import System
import ReadNum

guess : (target : Nat) -> (guesses : Nat) -> IO ()
guess target guesses = do
  putStr $ "Your guess (" ++ show guesses ++ " guesses so far): "
  Just g <- readNumber
  | Nothing => do putStrLn "Don't understand. Not a number?"
                  guess target guesses
  case (compare g target) of
       LT => do putStrLn "Too low!"
                guess target (S guesses)
       GT => do putStrLn "Too high!"
                guess target (S guesses)
       EQ => do putStrLn "Good guess, you win!"
                pure ()

main : IO ()
main = do
  t <- time
  let target = fromInteger . (1 + ) $ t `mod` 99
  guess target Z
