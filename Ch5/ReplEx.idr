module ReplEx

-- Ch5.2.3 Ex4

myReplWith : a -> String -> (a -> String -> Maybe (String, a)) -> IO ()
myReplWith acc prompt fn = do
  putStr prompt
  inp <- getLine

  case fn acc inp of
       Nothing => pure ()
       Just (m', i') => do
         putStr m'
         myReplWith i' prompt fn

myRepl : String -> (String -> String) -> IO ()
myRepl prompt fn = do
  putStr prompt
  inp <- getLine
  putStrLn $ fn inp
  myRepl prompt fn


sumInputs : Integer -> String -> Maybe (String, Integer)
sumInputs tot inp =
  let val = cast inp in
    if val < 0
       then Nothing
       else let newVal = tot + val in
                Just ("Subtotal: " ++ show newVal ++ "\n", newVal)

myReplWithTest : IO ()
myReplWithTest = myReplWith 0 "Value: " sumInputs

exclamationize : String -> String
exclamationize x = x ++ "!"

myReplTest : IO ()
myReplTest = myRepl "Make this more dramatic: " exclamationize
