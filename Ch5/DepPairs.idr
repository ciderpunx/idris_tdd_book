import Data.Vect
import Data.String
import System.File

-- Dependent pairs are like tuples, but the second elt can be computed from 
-- the /value of the first. Use ** as a comma
rainbowVect : (n:Nat ** Vect n String)
rainbowVect = (3 ** ["Zippy", "George", "Bungle"])
--rainbowVect = (3 ** ?something)


-- Now we can reimplement readVectOfUnknownLength like this
readVectOfUnknownLength : IO (len ** Vect len String)
readVectOfUnknownLength = do
  putStr "Value: "
  x <- getLine
  if (x=="")
     then pure (_ ** [])
     else do
       (_ ** xs) <- readVectOfUnknownLength
       pure ( _ ** x :: xs)

-- Now we want to
-- 1. Read 2 inpout vectors
-- 2. If they have different lengths, display an error
-- 3. Otherwise display the result of zipping them together

-- This is implemented as a single function.
zipInputs : IO ()
zipInputs = do
  putStrLn "Vector 1 (blank line to end): "
  (len1 ** vec1) <- readVectOfUnknownLength
  putStrLn "Vector 2 (blank line to end): "
  (len2 ** vec2) <- readVectOfUnknownLength
  case exactLength len1 vec2 of
     Nothing => putStrLn $ "Error: Must be the same length. Got: "
                ++ show len1 ++ ", " ++ show len2
     Just vec2'  => printLn (zip vec1 vec2')


-- Ex.5.3.4.1
readToBlank : IO (List String)
readToBlank = do
  putStr "Value: "
  x <- getLine
  if (x == "")
     then pure []
     else do
       xs <- readToBlank
       pure $ x :: xs

-- Ex. 5.3.4.2
writeLinesToFile : File -> List String -> IO ()
writeLinesToFile fh [] = pure ()
writeLinesToFile fh (x :: xs) = do
  Right x <- fPutStrLn fh x
  | Left err => printLn err
  writeLinesToFile fh xs

readAndSave : IO ()
readAndSave = do
  lines <- readToBlank
  putStr "Filename: "
  filename <- getLine
  Right fh <- openFile filename Append
  | Left err => printLn err
  writeLinesToFile fh lines
  closeFile fh
  putStrLn $ "Wrote to file: " ++ filename

-- Ex 5.3.4.3
-- This is slightly fancier than the spec because I show an error message
-- on failure to read the file. I also use readFile instead of fGetLine
-- Because it is nicer.
readVectFile : (filename : String) -> IO (n ** Vect n String)
readVectFile filename = do
    Right ls <- readFile filename
    | Left err => vecErr err
    pure (_ ** (fromList $ lines ls))
  where
    vecErr : FileError -> IO (n ** Vect n String)
    vecErr e = do
      putStrLn $ "Error: " ++ show e
      pure (_ ** [])

