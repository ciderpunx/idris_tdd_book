import Data.Vect

-- I changed the name of this function
-- This is fine if we know in advance the length of the Vect
readVectOfLen : (len : Nat) -> IO (Vect len String)
readVectOfLen Z = pure []
readVectOfLen (S k) = do
  putStr "Value: "
  x <- getLine
  xs <- readVectOfLen k
  pure (x::xs)


-- Otherwise we use a datatype, and Idris's type inference to
-- for eg. get lkines until an empty line is input
data VectUnknown : Type -> Type where
  MkVect : (len : Nat) -> Vect len a -> VectUnknown a

readVectOfUnknownLen : IO (VectUnknown String)
readVectOfUnknownLen = do
  putStr "Value: "
  x <- getLine
  if (x == "")
     then pure $ MkVect _ []
     else do
       MkVect _ xs <- readVectOfUnknownLen
       pure $ MkVect _ (x :: xs)

-- Because printLn doesn't know about VectUnknowns and maybe we
-- can't derive show. Now we can
-- :exec readVectOfUnknownLen >>= printVectUnknown
printVectUnknown : Show a => VectUnknown a -> IO ()
printVectUnknown (MkVect len xs) = do
  putStrLn $ show xs ++ " (Length: " ++ show len ++ ")"
