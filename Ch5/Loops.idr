module Main

import System
import ReadNum

total
countdown : (secs : Nat) -> IO ()
countdown Z = putStrLn "Lift off!"
countdown (S secs) = do
  putStrLn (show (S secs))
  usleep 1000000
  countdown secs

countdowns : IO ()
countdowns = do
  putStr "Start num: "
  Just startNum <- readNumber
  | Nothing => do putStrLn "Don't understand"
                  countdowns
  countdown startNum
  putStr "Another (y/n)? "
  yn <- getLine
  if yn == "y"
     then countdowns
     else pure()
