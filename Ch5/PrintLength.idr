printLength : IO ()
printLength =
  do putStr "Inp: "
     inp <- getLine
     let len = length inp
     putStrLn $ "Length: " ++ show len
