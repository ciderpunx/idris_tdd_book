-- Ex 5.1,2

longerStringLength : String -> String -> Nat
longerStringLength str1 str2 = max (length str1) (length str2)

printLonger : IO ()
printLonger =
  do putStr "String 1: "
     str1 <- getLine
     putStr "String 2: "
     str2 <- getLine
     let lsl = longerStringLength str1 str2
     putStrLn $ "The longer string is " ++ show lsl

printLonger' : IO ()
printLonger' =
  putStr "String 1: " >>=
    \_ => getLine >>=
    \str1 => putStr "String 2: " >>=
    \_ => getLine >>=
    \str2 =>
        let lsl = longerStringLength str1 str2 in
          putStrLn $ "The longer string is " ++ show lsl
