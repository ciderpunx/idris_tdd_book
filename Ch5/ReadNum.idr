module ReadNum
-- Can call like this in REPL
-- :exec readNumber >>= printLn

import Data.String

export
readNumber : IO (Maybe Nat)
readNumber = do
  inp <- getLine
  if all isDigit (unpack inp)
     then pure (Just (stringToNatOrZ inp)) -- see: https://idris2.readthedocs.io/en/latest/typedd/typedd.html#chapter-5
     else pure Nothing

readNumbers : IO (Maybe (Nat, Nat))
readNumbers = do
  n1 <- readNumber
  n2 <- readNumber
  case (n1,n2) of
       (Just x, Just y) => pure $ Just (x, y)
       ( _ , _ )        => pure Nothing

-- Actually there is a slightly neater pattern matching syntax
partial
readNumbers' : IO (Maybe (Nat, Nat))
readNumbers' = do
  Just n1 <- readNumber | Nothing => pure Nothing
  Just n2 <- readNumber | Nothing => pure Nothing
  pure (Just (n1,n2))
