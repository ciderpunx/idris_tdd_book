module Expr

-- Note Idris 2 can't infer the type in expressions like
-- the (Expr _) (6 + 3 * 12)

-- We get this:
-- Error: Can't find an implementation for Num (Expr ?_).

-- So instead tell it the type:
-- the (Expr Double) (6 + 3 * 12)
-- Add (Val 6.0) (Mul (Val 3.0) (Val 12.0))

-- Also eval (1 + 1) won't work, we have to specify the types like in
-- evalFuckingStupidREPL

data Expr num = Val num
              | Add (Expr num) (Expr num)
              | Sub (Expr num) (Expr num)
              | Mul (Expr num) (Expr num)
              | Div (Expr num) (Expr num)
              | Abs (Expr num)

Num ty => Num (Expr ty) where
    (+) = Add
    (*) = Mul
    fromInteger = Val . fromInteger

Neg ty => Neg (Expr ty) where
    negate x = 0 - x
    (-)      = Sub

Abs ty => Abs (Expr ty) where
    abs = Abs

eval : (Neg num, Integral num, Abs num) => Expr num -> num
eval (Val x) = x
eval (Add x y) = eval x + eval y
eval (Sub x y) = eval x - eval y
eval (Mul x y) = eval x * eval y
eval (Div x y) = eval x `div` eval y
eval (Abs x)   = abs (eval x)

evalFuckingStupidREPL : Expr Integer -> Integer
evalFuckingStupidREPL = eval

-- Ex 7.2.1

-- I wanted to save some typing like this. 
-- But this somehow makes idris think the function is not total due to recursive path

--shop : Show ty => String -> ty -> ty -> String
--shop op x y = "(" ++ show x ++ op ++ show y ++ ")"

--Show num => Show (Expr num) where
--  show (Val x)   = show x
--  show (Add x y) = shop "+" x y
--  show (Sub x y) = shop "-" x y
--  show (Mul x y) = shop "*" x y
--  show (Div x y) = shop "/" x y
--  show (Abs x) = "|" ++ show x ++ "|"

-- But writing an ugly version works.
Show ty => Show (Expr ty) where
  show (Val x) = show x
  show (Add x y) = "(" ++ show x ++ " + " ++ show y ++ ")"
  show (Sub x y) = "(" ++ show x ++ " - " ++ show y ++ ")"
  show (Mul x y) = "(" ++ show x ++ " * " ++ show y ++ ")"
  show (Div x y) = "(" ++ show x ++ " / " ++ show y ++ ")"
  show (Abs x)   = "|" ++ show x ++ "|"

-- Ex 7.2.2
(Abs ty, Integral ty, Neg ty, Eq ty) => Eq (Expr ty) where
  (==) x y = eval x == eval y

-- Ex 7.2.3
(Abs ty, Integral ty, Neg ty, Eq ty) => Cast (Expr ty) ty where
  cast = eval
