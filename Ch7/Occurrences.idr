occurrences : Eq ty => (item : ty) -> (values : List ty) -> Nat
occurrences item [] = 0
occurrences item (v :: values) = case v == item of
                                      False => occurrences item values
                                      True => 1 + occurrences item values

-- this is a bit nicer IMO
oc' : Eq ty => (item : ty) -> (values : List ty) -> Nat
oc' item values =
  foldl (\acc,v => if v == item then acc + 1 else acc ) 0 values

data Matter = Solid | Liquid | Gas

Eq Matter where
  (==) Solid Solid    = True
  (==) Liquid Liquid  = True
  (==) Gas Gas        = True
  (==) _ _            = False
-- No deriving Eq so far...
