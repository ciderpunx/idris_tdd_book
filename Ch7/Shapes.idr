import Data.List

data Shape =
             Triangle Double Double
             | Rectangle Double Double
             | Circle Double

area : Shape -> Double
area (Triangle base height) = 0.5 * base * height
area (Rectangle x y) = x * y
area (Circle radius) = pi * radius * radius

-- Ch7 Ex1
Eq Shape where
   (==) (Circle x) (Circle y)             = x==y
   (==) (Circle _) _                      = False
   (==) (Triangle x y) (Triangle x' y')   = x==x' && y==y'
   (==) (Triangle _ _) _                  = False
   (==) (Rectangle x y) (Rectangle x' y') = x==x' && y==y'
   (==) (Rectangle _ _) _                 = False

-- Ch7 Ex2
-- I went a bit beyond the spec and figured out what should happen when
-- different shapes of same area were encountered - Triangle < Circle < Rectangle
Ord Shape where
  compare s1 s2 =
    case compare (area s1) (area s2) of
       EQ => case (s1,s2) of
                  ((Triangle _ _), (Triangle _ _))   => EQ
                  ((Triangle _ _), _)                => LT
                  ((Circle _), (Circle _ ))          => EQ
                  ((Circle _), (Triangle _ _))       => GT
                  ((Circle _), (Rectangle _ _))      => LT
                  ((Rectangle _ _), (Rectangle _ _)) => EQ
                  ((Rectangle _ _), _)               => GT
       LT => LT
       GT => GT

testShapes : List Shape
testShapes = [Circle 3, Triangle 3 9, Rectangle 2 6, Circle 4, Rectangle 2 7]

testEqAreaShapes : List Shape
testEqAreaShapes = [Circle 1, Rectangle 3.141592653589793 1, Circle 1]
