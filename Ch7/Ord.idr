import Data.List

record Album where
  constructor MkAlbum
  artist : String
  title : String
  year : Integer

Eq Album where
  (==) (MkAlbum a t y) (MkAlbum a' t' y') =
    a == a' && t == t' && y == y'

Ord Album where
  compare (MkAlbum a t y) (MkAlbum a' t' y') =
    case compare a a' of
         EQ => case compare y y' of
                    EQ => compare t t'
                    diff_year => diff_year
         diff_artist => diff_artist

Show Album where
  show (MkAlbum a t y) =
    "'" ++ t ++ "'" ++ ", by " ++ a ++ " (released " ++ show y ++ ")"

help : Album
help = MkAlbum "The Beatles" "Help" 1965

rs : Album
rs = MkAlbum "The Beatles" "Rubber Soul" 1965

clouds : Album
clouds = MkAlbum "Joni Mitchell" "clouds" 1969

collection : List Album
collection = [help, rs, clouds]
