--import Data.Vect as V

-- Vect is /indexed/ by length and /parameterized/ by an element type
-- parameter stays the same over the structure (any elt of a vector has the same tyoe)
-- index can change - eg. a subvector can have a different length
data Vect : Nat -> Type -> Type where
  Nil  : Vect Z a
  (::) : (x : a) -> (xs : Vect k a) -> Vect (S k) a
  -- i.e. appending an element x to a k-length vect
  -- gives a vect of length succ(k)
%name Vect xs, ys, zs

append : Vect n a -> Vect m a -> Vect (n + m) a
append [] ys = ys
append (x :: xs) ys = x :: append xs ys

emptyVect : Vect Z a
emptyVect = []

zip : Vect n a -> Vect n b -> Vect n (a,b)
zip [] [] = []
zip (x :: xs) (y :: ys) = (x, y) :: zip xs ys

-- Fin is the type of finite numbers with an upper bound of n
-- This causes namespace problems when importing Data.Vect so comment out. 
-- I get the idea
--tryIndex : {n : _} -> Integer -> Vect n a -> Maybe a
--tryIndex {n} i xs = case V.integerToFin i n of
--                      Nothing => Nothing
--                      Just idx => Just (V.index idx xs) 

-- Ex 4.2.3/4
-- We get a somewhat different type error from the book, there's an arguably better imp
-- in VectTake.idr
vectTake : (n : Nat) -> Vect (n + m) a -> Vect n a
vectTake Z xs = []
vectTake (S k) (x :: xs) = x :: vectTake k xs

sumEntries : Num a => (pos : Integer) -> Vect n a -> Vect n a -> Maybe a
sumEntries 0 [] _ = Nothing
sumEntries 0 (x :: xs) (y :: ys) = Just (x + y)
sumEntries pos (x :: xs) ( y :: ys) = sumEntries (pos - 1) xs ys
sumEntries _ [] _ = Nothing -- compiler insists??

sumEntriesTest1 = sumEntries 2 [1,2,3,4] [5,6,7,8] == Just 10
sumEntriesTest2 = sumEntries 4 [1,2,3,4] [5,6,7,8] == Nothing
