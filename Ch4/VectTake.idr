import Data.Vect

-- Ex 4.2.3/4
-- This is an implementation of Ex 4.2.3.4 using Fin which in theory may be better.
vectTake' : (m : Fin (S n)) -> Vect n a -> Vect (finToNat m) a
vectTake' FZ _ = []
vectTake' (FS k) ( x::xs ) = x :: vectTake' k xs
