-- Union type (like enumeration, but constructors can carry data)
-- Note how we do the documentation (you can use :doc Shape to see it)

||| Represents shapes
data Shape =
             ||| A triangle with base length and height
             Triangle Double Double
           | ||| A rectangle with its length and height
             Rectangle Double Double
           | ||| A circle with its radius
             Circle Double

area : Shape -> Double
area (Triangle base height) = 0.5 * base * height
area (Rectangle x y) = x * y
area (Circle radius) = pi * radius * radius -- cf. here for why no ^2 https://github.com/idris-lang/Idris2/issues/592


-- We also have another way to do declarations which is a bit more general 
-- and wordy and will be used when working with Dependent types
data Shape' : Type where
  Triangle' : Double -> Double -> Shape'
  Rectangle' : Double -> Double -> Shape'
  Circle' : Double -> Shape'


-- We can have recursive types - defined in terms of themselves like Nat
-- Usin this we can build a more complex Picture type that is either a pic
-- or a combo or a roatation or a translation
data Picture = Primative Shape
             | Combine Picture Picture
             | Rotate Double Picture
             | Translate Double Double Picture

circle : Picture
circle = Primative (Circle 4)

rectangle : Picture
rectangle = Primative (Rectangle 4 4)

triangle : Picture
triangle = Primative (Triangle 3 3)

testPicture : Picture
testPicture = Combine (Translate 5 5 rectangle) 
  (Combine (Translate 25 5 circle)
           (Translate 15 25 triangle))

pictureArea : Picture -> Double
pictureArea (Primative x) = area x
pictureArea (Combine x y) = pictureArea x + pictureArea y
pictureArea (Rotate x y) = pictureArea y
pictureArea (Translate x y z) = pictureArea z

-- Ch4 ex 6

-- testdata
testPic1 : Picture
testPic1 = Combine (Primative (Triangle 2 3))
                   (Primative (Triangle 2 4))

testPic2 : Picture
testPic2 = Combine (Primative (Rectangle 1 3))
                   (Primative (Circle 4))

biggestTriangleArea : Picture -> Maybe Double
biggestTriangleArea (Primative t@(Triangle x y)) = Just $ area t
biggestTriangleArea (Primative (Rectangle x y)) = Nothing
biggestTriangleArea (Primative (Circle x)) = Nothing
biggestTriangleArea (Rotate x y) = biggestTriangleArea y
biggestTriangleArea (Translate x y z) = biggestTriangleArea z
biggestTriangleArea (Combine x y) =
  case ((biggestTriangleArea x), (biggestTriangleArea y)) of
     (Nothing, Nothing) => Nothing
     (Nothing, (Just ya)) => Just ya
     ((Just xa), Nothing) => Just xa
     ((Just xa), (Just ya)) => case compare xa ya of
         LT => Just ya
         _ => Just xa
