-- Generic types
-- Need: eg. Maybe for catching error, Either, List, etc.
-- binary tree is a good eg.

-- note % naming hint for interactive definition help

data Tree elt = Empty | Node (Tree elt) elt (Tree elt)
%name Tree left, right

insert : Ord elt => elt -> Tree elt -> Tree elt
insert x Empty = Node Empty x Empty
insert x ts@(Node left val right) = case compare x val of
                                    LT => Node (insert x left) val right
                                    EQ => ts -- If the value is already in the tree we may as well just return the original tree`
                                    GT => Node left val (insert x right)

-- We can add the ord constraint to the type defn itself like this:
data BSTree : Type -> Type where
  BEmpty : Ord elt => BSTree elt
  BNode : Ord elt => 
          (left : BSTree elt) ->
          (val : elt) ->
          (right : BSTree elt) ->
          BSTree elt

binsert : elt -> BSTree elt -> BSTree elt
binsert x BEmpty = BNode BEmpty x BEmpty
binsert x ts@(BNode left val right) = case compare x val of
                                    LT => BNode (binsert x left) val right
                                    EQ => ts -- If the value is already in the tree we may as well just return the original tree`
                                    GT => BNode left val (binsert x right)

-- Ch4. Ex 1
listToTree : Ord a => List a -> Tree a
listToTree [] = Empty
listToTree (x :: xs) = insert x (listToTree xs)

-- Ch4 ex2
treeToList : Tree a -> List a
treeToList Empty = []
treeToList (Node left x right) =  treeToList left ++ x :: (treeToList right)
