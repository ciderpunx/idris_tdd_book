data Expr =
   Sint Int
   | Add Expr Expr
   | Sub Expr Expr
   | Mul Expr Expr

evaluate : Expr -> Int
evaluate (Sint x) = x
evaluate (Add x y) = evaluate x + evaluate y
evaluate (Sub x y) = evaluate x - evaluate y
evaluate (Mul x y) = evaluate x * evaluate y

-- We don't seem to be able to use a generic type here
-- in the Nothing Nothing case - We get:
-- Error: Can't find an implementation for Ord ?a.
-- No solution forthcoming here:
-- https://github.com/edwinb/TypeDD-Samples/issues/5
maxMaybe : Ord a => Maybe a -> Maybe a -> Maybe a
maxMaybe Nothing (Just x) = Just x
maxMaybe Nothing Nothing = Nothing
maxMaybe (Just x) Nothing = Just x
maxMaybe (Just x) (Just y) = Just $ max x y
