module Main

import Data.Vect
import Data.String
import System.REPL

data DataStore : Type where
  MkData : (size : Nat) ->
           (items : Vect size String) ->
           DataStore

data Command = Add String
             | Get Integer
             | Search String
             | Size
             | Quit

size : DataStore -> Nat
size (MkData s _) = s

items : (store : DataStore) -> Vect (size store) String
items (MkData _ is) = is

addToStore : DataStore -> String -> DataStore
addToStore (MkData size items) y = 
    MkData _ (addToData items)
  where
    addToData : Vect old String -> Vect (S old) String
    addToData [] = [y]
    addToData (item :: items) = item :: addToData items

getEntry : (pos : Integer) -> (store : DataStore) -> Maybe (String, DataStore)
getEntry pos store = let store_items = items store in
                         case integerToFin pos (size store) of
                              Nothing => Just (show pos ++ " is out of range\n", store)
                              Just id => Just ("Got item: " ++ index id store_items ++ "\n", store)

searchStore : (needle : String) -> (store : DataStore) -> Maybe (String, DataStore)
searchStore needle store =
  let storeItems = items store in
    Just ((showItemsMatching 0 needle storeItems), store)

showItemsMatching : Integer -> String -> Vect n String -> String
showItemsMatching idx term [] = ""
showItemsMatching idx term (x :: xs) =
    matchStr ++ (showItemsMatching (idx + 1) term xs)
  where
    matchStr : String
    matchStr = case isInfixOf term x of
       True => (show idx) ++ ": " ++ x ++ "\n"
       False => ""

parseCmd : (cmd : String) -> (args : String) -> Maybe Command
parseCmd "add" str = Just (Add str)
parseCmd "get" val = case all isDigit (unpack val) of
                          False => Nothing
                          True => Just (Get (cast val))
parseCmd "search" str = Just (Search str)
parseCmd "size" _ = Just Size
parseCmd "quit" _ = Just Quit
parseCmd _ _ = Nothing

parse : (inp : String) -> Maybe Command
parse inp = case span (/= ' ') inp of
                 (cmd, args) => parseCmd cmd (ltrim args)

doCmd : DataStore -> Command -> Maybe (String, DataStore)
doCmd store (Add item) =
  Just ("ID " ++ show (size store) ++ "\n", addToStore store item)
doCmd store (Get item) = getEntry item store
doCmd store (Search term) = searchStore term store
doCmd store Size =
  Just ("Store size is: " ++ show ( size store ) ++ "\n", store)
doCmd store Quit = Nothing

processInput : DataStore -> String -> Maybe (String, DataStore)
processInput store inp = case parse inp of
                              Nothing => Just ("Invalid command (eg. Add something | Get 2 | Size | Quit)\n", store)
                              Just cmd => doCmd store cmd

main : IO ()
main = replWith (MkData _ []) "Command: " processInput
